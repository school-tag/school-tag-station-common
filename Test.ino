/** Tests for TagIo interaction with the MFRC522 RFID reading and writing to a
 * school tag.
 *
 * See Tag.io for School Tag (NFC NTAG 213) requirements, which these tests use
 * to demonstrate the use of the library will work properly.
 *
 * See the school-tag-remote-station-arduino repository for proper setup of the
 * Arduino used to test.
 *
 * Usage:
 * - Open the `Serial Monitor` in the Arduino IDE
 * - Run the tests by holding the Tag to the RFID reader until the tests
 * complete.
 * - Output will indicate Pass or Failure (tests short circuit if any failure)
 * - Success: 1 second LED pulse
 * - Faiure: 1 second of 100ms LED flashes
 * - Tests can be repeated when a different tag is used or with the same tag
 * after 1 minute.
 *
 * Tasks:
 * - Test scenario when tag has max visits written
 * - Add tone to success / failure
 */
#include "src/StationSound.h"
#include "src/TagIo.h"

#include <Arduino.h>
#include <SPI.h>

#define SS_PIN 10
#define RST_PIN 6
#define BUZZER_PIN 8

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance

TagIo io(mfrc522);
StationSound sound(BUZZER_PIN);

void setup() {
  Serial.begin(115200); // Initialize serial communications with the PC
  SPI.begin();          // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522 card

  Serial.println(F("Hold tag to RFID reader to run tests."));

  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  if (io.tagIsReady()) {
    if (!io.tagIsNew()) {
      sound.ofRepeatTag();
      return;
    }
  } else {
    return;
  }
  Serial.print(F("===================="));
  Serial.print(F(" Tag ID: "));
  Serial.print(io.uid());
  Serial.println(F(" ===================="));
  bool passed = test("Tag is not new", tagIsNotNew()) &&
                test("Wipe reports zero visits", wipeReportsZeroVisits()) &&
                test("Visit read equals visit written",
                     readingVisitWrittenShouldBeEqual()) &&
                test("3 visits can be written and printed", write3Visits()) &&
                test("max visit clobbers prior visit",
                     maxVisitWrittenClobbersPriorVisit()) &&
                test("reading out of bounds returns empty",
                     readingNonExistentVisitReturnsEmpty()) &&
                test("battery keys are correct", batteryLevelKeys());
  test("did sound pulse 3 times?", soundOfSuccessBlocksUntilFinished());

  if (passed) {
    Serial.println("********* Pass ************ ");
    digitalWrite(LED_BUILTIN, HIGH);
    // TODO: Figure out why the light pulse is just a flash and not as long as
    // the delay
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
  } else {
    Serial.println("!!!!!!!!!!! FAILED !!!!!!!!!!!!");
    for (int i = 0; i < 5; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(100);
    }
  }
}

//======= tests =========

/**
 * Basic test demonstrating we can wipe and read the number of records present.
 */
bool wipeReportsZeroVisits() {
  bool passed = io.wipe();
  return passed && io.numberOfVisits() == 0;
}

/**
 * demonstrates what is read is the same that is written.
 */
bool readingVisitWrittenShouldBeEqual() {
  TagIo::Visit visit = TagIo::Visit();
  visit.timestamp = 0x898;
  visit.stationId = 0x2B;
  visit.batteryLevel = TagIo::BatteryLevel::VOLTAGE_HIGH;
  visit.version = 7;
  bool passed = assert("able to wipe in preparation", io.wipe());
  if (passed) {
    passed = assertEquals("able to write first visit", 0, io.writeVisit(visit));
  }
  if (passed) {
    int count = io.numberOfVisits();
    passed = assert("first record counts", count == 1);
  }
  if (passed) {
    TagIo::Visit visitRead = io.readVisit(0);
    passed = assertEquals("station id matches", visit.stationId,
                          visitRead.stationId);
    passed = assertEquals("timestamp matches", visit.timestamp,
                          visitRead.timestamp) &&
             passed;
    passed = assertEquals("battery level matches", visit.batteryLevel,
                          visitRead.batteryLevel) &&
             passed;
    passed =
        assertEquals("version", visit.version, visitRead.version) && passed;
  }

  return passed;
}

/**
 * Demonstrates that multiple visits can be written
 */
bool write3Visits() {
  bool passed = assert("wiped", io.wipe()) &&
                assertEquals("first", 0, io.writeVisit({0x01, 0x01})) &&
                assertEquals("second", 1, io.writeVisit({0x02, 0x02})) &&
                assertEquals("third", 2, io.writeVisit({0x03, 0x03}));

  passed =
      passed && assertEquals("three visits written", 3, io.numberOfVisits());
  io.print(); // nothing to test here beyond return
  return passed;
}

/**validates keys match their enum
 */
bool batteryLevelKeys() {
  return assertBatteryLevelKey("recharge", TagIo::BatteryLevel::RECHARGE) &&
         assertBatteryLevelKey("low", TagIo::BatteryLevel::VOLTAGE_LOW) &&
         assertBatteryLevelKey("medium", TagIo::BatteryLevel::VOLTAGE_MEDIUM) &&
         assertBatteryLevelKey("high", TagIo::BatteryLevel::VOLTAGE_HIGH);
}

bool assertBatteryLevelKey(String expectedKey,
                           TagIo::BatteryLevel batteryLevel) {
  String actual = io.batteryLevelKey(batteryLevel);
  if (expectedKey == actual) {
    return true;
  } else {
    Serial.print("expected ");
    Serial.print(expectedKey);
    Serial.print(" but was ");
    Serial.println(actual);
    return false;
  }
}
/**
 * Edge case when a student visits too many stations without visiting the school
 * station. The visit is written to the last record cloberring the most recent
 * visit. The logic here is that the student may be repeatedly tagging in at the
 * same station. Kids to the darndest things!
 *
 * An alternate strategy would rotate to the beginning and clobber the start,
 * but then the repeat tagger at the same station could wipe all other records
 * with the same repeat.
 *
 * Alternate strategies of finding the same station in the record list and
 * overwriting could protect, but keeping this alternate scenario simple since
 * it is a the unusually case.
 *
 * Giving a student "warning" feedback when this is happening may curb unusual
 * behavior of over tagging or not tagging in at the school where the wipe
 * happens.
 */
bool maxVisitWrittenClobbersPriorVisit() {
  bool pass = assert("wiped", io.wipe());
  int maxVisitCount = 5; // See TagIo::MAX_RECORDS
  int expectedMaxWriteIndex =
      maxVisitCount - 1; // should repeat at the last index
  int writeIndex;
  for (unsigned int i = 0; i < maxVisitCount; i++) {
    writeIndex = io.writeVisit({i, i});
    pass = pass && assertEquals("written to expected record " + String(i, DEC),
                                i, writeIndex);
  }
  // max records written...this the clobber step
  if (pass) {
    TagIo::Visit visitOut = {0x77, 0x88};
    int writeIndex = io.writeVisit(visitOut);
    pass = pass && assertEquals("expected to clobber last record index",
                                expectedMaxWriteIndex, writeIndex);
    TagIo::Visit visitIn = io.readVisit(expectedMaxWriteIndex);
    pass = pass && assertEquals("visit overwrites previous", visitOut.timestamp,
                                visitIn.timestamp);
  }
  return pass;
}

/**Demonstrates proper error reporting for a bad read.*/
bool readingNonExistentVisitReturnsEmpty() {
  TagIo::Visit emptyVisit = TagIo::Visit();
  return assert("wiped", io.wipe()) &&
         assertEquals("reading index zero when none exist",
                      emptyVisit.stationId, io.readVisit(0).stationId);
}

/**Calling tagIsNew again will return false since it was called to enter the
 * tests.*/
bool tagIsNotNew() { return !io.tagIsNew(); }
/**
 * Sounds the beep of success to test tones.  Test verifies blocking
 * requirements, but does not ensure sound is successful ... that requires human
 * observation.
 */
bool soundOfSuccessBlocksUntilFinished() {
  unsigned long start = millis();
  sound.ofSuccessfulVisit(
      3); // listen for pulse...no way to test programatically
  unsigned long finish = millis();
  unsigned long durationInMillis = finish - start;
  // the beep lasts 1 second, but can be off slightly due to processing times.
  // provie a big cushion
  return assert("sound blocked as expected: " + String(durationInMillis),
                durationInMillis >= 900 && durationInMillis <= 1100);
}
//====== test utilities ==========
bool test(String description, bool passed) {
  String line = "------ ";
  line.concat(description);
  line.concat(" ------\n");
  return assert(line, passed);
}
bool assertEquals(String description, long expected, long actual) {
  if (!assert(description, expected == actual)) {
    Serial.print(F("expected : "));
    Serial.println(expected, HEX);
    Serial.print(F("actual   : "));
    Serial.println(actual, HEX);
    return false;
  };
  return true;
}

/**Standard reporting of test results*/
bool assert(String description, bool passed) {
  if (!passed) {
    Serial.print(F("!!!FAILED!!! -> "));
  }
  Serial.println(description);
  if (!passed) {
    io.print();
  }
  return passed;
}