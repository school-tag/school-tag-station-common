# School Tag Station - Common

C++ code compatible with Arduino and Particle.io platforms.

Test.ino is an Arduino sketch that demonstrates and tests features interacting with an NFC School Tag. The ino file is not intended for use with other sketches.

## How To Use

You will interact with TagIo (Tag Input/Output) that 
interacts with the MFRC522 RFID library to interact with the tag.  All features cater to the School Tag 
scenarios.

StationSound provides audible feedback when connected to a buzzer.  

See TagIo.h and StationSound.h for more documentation.

### TagIo Initialization

TagIo is a stateful class that uses the MFRC522 to communicate with the NFC tag.  Instantiate and initialize it once in your sketch.

```
#define SS_PIN 10
#define RST_PIN 6

MFRC522 mfrc522(SS_PIN, RST_PIN);
TagIo io(mfrc522);

void setup()
{
    mfrc522.PCD_Init(); // Init MFRC522 card
}
```

### Listen for New Tags

The student holds the tag near the MRFC522 RFID reader. 
The loop is repeatly checking for when a new tag arrives.  The student may hold the tag for several seconds without concern for writing multiple tags.

TagIo and MFRC522 interact with a single tag at a time. 

```
void loop()
{
    if (!io.newTagIsReady())
    {
        return;
    }
    io.print();
}
```


### Write Visit to School Tag

The station records the student's visit on to the School Tag.  

There is a max number of records (declared in TagIo.h)since the NFC tags have very limited data sizes so if a student's tag is full then the last record will be clobbered. This wouldn't happen in normal scenarios.

```
TagIo::Visit visit = TagIo::Visit();
visit.timestamp = {unix time};
visit.stationId = {unique id for station};
io.writeVisit(visit);
```

### Read Visits from School Tag

The School Station reads all the visits from the remote stations to reward the student for tagging in along the Safe Routes to School.  

Get the number of visits recorded and read them by 
zero-based index.  

```
int count = io.numberOfVisits();

for(int i = 0; i < count; i++){
    Visit visit = io.readVisit(i);
    doSomethingWith(visit.timestamp);
    doSomethingElseWith(visit.stationId);
}

//all visits retrieved...clearing out for the next game
io.wipe();
```

### Play a Sound 

Sound provides feedback to the student visiting a station.  The library abstracts sounds into game 
scenarios to provide consistency across devices and keep client code simple.  Unlike the tone() function
in Arduino, these calls are blocking so they should be called at the end of the loop since the tag may not 
be available after the sound.

See StationSound.h for more documentation.

```
#define BUZZER_PIN 8
StationSound sound(BUZZER_PIN);

loop(){
    sound.ofSuccessfulVisit(io.numberOfVisits());
}
```
### Run the Tests

The tests are useful to demonstrate how to use the library and for testing hardware.  Simply run the
Test.ino file in an IDE and open the Serial Monitor to 
look for the output reported.

```
==================== Tag ID: 040D8932F14A80 ====================
------ Wipe reports zero visits ------

able to wipe in preparation
able to write first visit
first record counts
station id matches
timestamp matches
------ Visit read equals visit written ------

...

wiped
reading index zero when none exist
------ reading out of bounds returns empty ------

********* Pass ************ 
```

## IDE Setup

### Arduino Library Setup 

The library must be included with your project for use. The procedure is different for Arduino and Particle.

#### Arduino IDE
This library is not available in the Library Manager so it must be handled as a custom library (see [docs](https://www.arduino.cc/en/hacking/libraries)).

For example, on a mac this repository should be cloned to:

`~/Documents/Arduino/libraries/`

#### VSCode + Arduino

See school-tag-station-remote-arduino project.

### Particle Library Setup

Particle requires libraries to be published to their cloud.  This allows their Cloud IDE to compile a library.  The process is the same regardless of the IDE you use (Workbench, Desktop, Web). 

#### Publish to Particle 

Use the [particle-cli](https://docs.particle.io/reference/developer-tools/cli/) to publish the library.

1. Increment the version
    1. Found in the library.properties
    1. Follow [semantic versioning](https://semver.org/)
    1. Also change the version in the repository url
1. Create a git Tag to match the version
    1. For references only
1. Publish to Particle with CLI
    1. `particle library publish`

TODO:
* Automate the release process for consistency
    * Input `version`
    * Input `changes sentence`
    * Update version in library.properties
    * Update Readme with Version and Changes sentence
    * Publish to particle
    * Tag git with version
    * Push to git 
    * Create Pull Request or Merge to Master TDB
    * Get Bitbucket Pipelines to do this for us?
    * Run the tests to verify it works before publishing
    
# Versions

* 1.0 - Initial library verified to read/write to NFC tags on Arduino and Particle
* 1.1 - Added sound to give feedback to the player for success/failure  
* 1.2
    * Added battery level reporting. 
    * Changed serial monitor baud to 115200
* 1.3
    * Added station software version to the visit
    * Added battery level keys for a human readable string
    