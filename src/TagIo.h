/*
 * This program works specifically with compatible School Tags (Ntag 213 with
 * 144 bytes). The student scans the tag and this station will log the visit to
 * the station recording:
 *  - The Station ID
 *  - The date and time of the visit
 *
 * When the student arrives to school, the data will be read from the tag and
 * the game will know which stations were scanned.
 *
 * School Tags have a single NDEF record which is the URL of the Players
 * Scoreboard:
 *
 * https://schooltag.org/tag/{UID}
 *
 * The visit data resides in binary format after the NDEF message, referenced by
 * START_PAGE_ADDR.
 *
 * -------------------------------
 * |      4 Byte Header          |
 * -------------------------------
 * | byte |   Description        |
 * -------------------------------
 * | 0    | Format version       |
 * -------------------------------
 * | 1    | Reserved             |
 * -------------------------------
 * | 2    | Record Count         |
 * -------------------------------
 * | 3    | Reserved             |
 * -------------------------------
 * | 4-n  | Records              |
 * -------------------------------
 *
 *
 * -------------------------------
 * |     8 Byte Record           |
 * -------------------------------
 * | 0-1    | Station ID         |
 * -------------------------------
 * | 2-3    | Station Vitals     |
 * | Bit 0,1| Battery Level      |
 * -------------------------------
 * | 4-7    | Unix Timestamp     |
 * -------------------------------
 *
 * Possibilities for Reserved bytes:
 * - Station battery life
 * - Number of station scans today
 */
#ifndef TagIo_h
#define TagIo_h
#endif

#include <MFRC522.h>

#define PAGE_SIZE 4
// reference to the 4 byte page where school tag visits start
#define START_PAGE_ADDR 22
// reference to the last 4 byte page where school tag visits are saved
#define FINAL_PAGE_ADDR 37
// used to wipe out unused data indicating no visit data is stored
#define RESET_CHAR 0xFF
// unique identifier for the start of the visit records
#define FORMAT_VERSION 0xBC
#define FORMAT_VERSION_INDEX 0
#define RECORD_COUNT_INDEX 2
#define MAX_RECORDS 5
// more readable than -1 when indicating failure for ints
#define FAILED -1
class TagIo {

public:
  enum BatteryLevel { RECHARGE, VOLTAGE_LOW, VOLTAGE_MEDIUM, VOLTAGE_HIGH };
  struct Visit {
    unsigned int stationId;    // uniquely identify a station
    unsigned long timestamp;   // the time of the visit
    BatteryLevel batteryLevel; // describe how much battery remains
    byte version;              // the software version on the station
  };

  /**Default constructor for an instance of the IO class.*/
  TagIo(void);
  /**The constructor that must be used to interact with the tags.
   * MFRC522 must be initialized during setup.  See Test.ino
   *
   *
   */
  TagIo(MFRC522 &rfid);

  /**Prints the contents of a tag to the Serial output.*/
  void print();

  /**Erases the tag of any data pertaining to the School Tag Data.*/
  bool wipe();

  /**
   * Indicates a tag is available for read/write.
   * Use newTagIsReady if wanting to avoid repeats.
   */
  bool tagIsReady();

  /** Indicates when a new tag is ready for interaction.
   * To avoid duplicate reads, this will return true only once per tag.
   * A tag may be repeated once another tag is read.
   *
   * tagIsReady must be called prior to calling this.
   *
   * @returns true if a new tag is detected.
   */
  bool tagIsNew();

  /**Records a visit onto a tag.  Adds it to the first empty record
   * by reading the header for existing record count.  Updates the header
   * to reflect the increased record count.
   * @returns the index of the record written or -1 if there was an error
   * */
  int writeVisit(Visit visit);

  /** Read the visit record located at the given index.
   *  @zeroBasedRecordIndex which record wishing to access.  between 0 -
   * numberOfVisits (exclusive)
   *  @returns the visit found at the given index or an empty Visit if the index
   * is out of bounds
   */
  Visit readVisit(unsigned int zeroBasedRecordIndex);

  /**
   * Reads the header potentially located at the start page and returns the
   * number of records as indicated.  If the version check does not match, then
   * 0 is returned. If there is a problem reading the tag, then -1 is returned.
   * @returns the number of visits found on the tag or -1 if unable to read
   * header
   */
  int numberOfVisits();

  /**Called when a tag has been detected.
   * @returns the UID of the tag, in uppercase HEX string.
   */
  String uid();

  /**
   * Given a BatteryLevel, this will return a human readable string that can be
   * used as a key to represent the corresponding level that is otherwise
   * transported as an int.
   *
   * @param batteryLevel of interest
   * @return String key representing the level similar to the enum name.
   */
  String batteryLevelKey(BatteryLevel batteryLevel);

private:
  MFRC522::StatusCode status; // variable to get card status

  /**Sateful copy of the data for reading and writing.
   *  (16+2 bytes data+CRC)
   */
  byte buffer[18];

  /** The uid of the tag most recently scanned to help
   * avoid duplicate writes.
   */
  String mostRecentTagUid;

  /** Calculates the page address given the index to the record of interest.
   * @zeroBasedRecordIndex reference to the record of interest
   * @return the offset from the start
   */
  static int pageAddress(int zeroBasedRecordIndex);

  /** The instance of the RFID reader/writer to be used for communications.*/
  MFRC522 *rfid;

  /** Reads the data from the tag into the buffer starting at the page address
   * given. Assigns the resulting status to the instance variable.
   *
   * @pageAddr the number of bytes from the beginning of the tag to read one
   * page of data (18 bytes).
   * @returns true if successfully read bytes
   */
  bool readIntoBuffer(byte pageAddr);

  /**Writes a 4 byte buffer to the page of storage starting at the given page
   * address. */
  bool writePage(uint8_t pageAddr, byte *pageData);

  /** useful for debugging, this prints the reason for a failed IO, nothing if
   * status is OK.
   * @returns
   */
  bool statusIsOk();
};
