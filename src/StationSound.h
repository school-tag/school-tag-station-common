/**
 * The School Tag Stations make sounds to give feedback
 * when a Student holds a tag to the station. Sounds are
 * made for the following reasons:
 * - Confirm the station is awake and listening
 * - Confirm the station is connected to the tag
 * - Provide feedback about the success/failure of communication
 * - Possibly for encouragement when special situations are achieved
 *   - Multiple students tag in together as a group (safety in numbers)
 *   - Significant achievement reached (fifth in a row, reached X miles, etc)
 *
 * This class does is not concerned with the logic about the reasons, but simply
 * offers the sounds that may be used to communicate such situations in a
 * cross-device format.
 *
 * Limitations:
 *  - Single buzzer
 *
 * Targeted Platforms:
 *  - Arduino -
 * https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/
 *  - Particle Argon ->
 * https://docs.particle.io/reference/device-os/firmware/argon/#tone-
 *
 * Both platforms use the same syntax:
 *
 *  tone(pin, frequency, duration)
 *
 *
 * Particle has restrictions about the pin:
 *  On Gen 3 Feather devices (Argon, Boron, Xenon), pins A0, A1, A2, A3, D2, D3,
 * D4, D5, D6, and D8 can be used for tone().
 *
 * The pin is provided upon construction leaving only frequency, duration to be
 * provided during the targeted sounds.
 *
 * Usage:
 *  #BUZZER_PIN D8
 *  StationSound sound(BUZZER_PIN);
 *  loop(){ sound.ofSuccessfulVisit(io.numberOfVisits());}
 *
 * All methods are blocking and will return only when the sound is complete
 * (even though the tone method does not). For this reason, the sound should be
 * located at the end of interaction since it is likely the student will remove
 * tag interaction during that time.
 */

class StationSound {
public:
  StationSound();
  /**
   * @pin of the buzzer connected to microcontroller
   */
  StationSound(int pin);

  /** The most common and important sound indicating the student has had a
   * succesful visit to a station.  The number of visits on the tag will be
   * communicated with a pulse count to confirm to the student how full their
   * tag is. This is subtle since there is little the student needs to know, but
   * it gives confirmation of progression towards school.
   *
   * Kids love to count and this also helps the student recognize patterns
   * helping them understand every station visit counts.
   *
   * If the tag is getting full, that is a different sound
   * @numberOfVisitsOnTag to communicate progress
   *
   * */
  void ofSuccessfulVisit(int numberOfVisitsOnTag);

  /**Indicates the tag read is the same tag as previous.*/
  void ofRepeatTag();

private:
  /**The pin where the sound maker will be found. */
  int buzzerPin;

  /** calls the tone() method blocking until the sound is complete.
   *
   * @frequency to affect the pitch
   * @durationInMillis how long the sound should last.
   */
  void sound(int frequency, int durationInMillis);
};