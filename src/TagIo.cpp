

#include "TagIo.h"

TagIo::TagIo(void) {
  // data transfer buffer (16+2 bytes data+CRC)
}

TagIo::TagIo(MFRC522 &mfrc522) { rfid = &mfrc522; }

int TagIo::numberOfVisits() {
  if (!readIntoBuffer(START_PAGE_ADDR)) {
    return FAILED;
  };
  unsigned int version = buffer[FORMAT_VERSION_INDEX];
  byte recordCount = 0;
  if (version == FORMAT_VERSION) {
    recordCount = buffer[RECORD_COUNT_INDEX];
  }
  return recordCount;
}

bool TagIo::tagIsReady() {
  // Look for new cards
  if (!rfid->PICC_IsNewCardPresent()) {
    return false;
  }

  // Select one of the cards
  if (!rfid->PICC_ReadCardSerial()) {
    return false;
  }
  return true;
}
bool TagIo::tagIsNew() {
  String currentUid = uid();
  if (mostRecentTagUid == currentUid) {
    return false;
  }
  mostRecentTagUid = currentUid;
  return true;
}

bool TagIo::readIntoBuffer(byte pageAddr) {
  byte size = sizeof(buffer);
  status = (MFRC522::StatusCode)rfid->MIFARE_Read(pageAddr, buffer, &size);
  return statusIsOk();
}

void TagIo::print() {

  // Show the Tag UID
  Serial.print(F("===================="));
  Serial.print(F(" Tag ID: "));
  Serial.print(uid());
  Serial.println(F(" ===================="));

  // Show the latest io status
  Serial.print("Status: ");
  Serial.println(rfid->GetStatusCodeName(status));

  int recordCount = numberOfVisits();

  if (recordCount < 0) {
    Serial.println(F("Failed to read record count"));
    return;
  }

  Serial.println();
  // show each visit
  {
    Serial.print(F("=============== "));
    Serial.print(recordCount, DEC);
    Serial.println(F(" visits ==============="));
    Serial.println(F("stationId\ttimestamp\tbattery\t\tversion"));
    Serial.println(F("---------\t---------\t-------\t\t-------"));
    Visit visit;
    for (int i = 0; i < recordCount; i++) {
      visit = readVisit(i);
      Serial.print(visit.stationId, DEC);
      Serial.print(F("\t\t\t"));
      Serial.print(visit.timestamp, DEC);
      Serial.print(F("\t\t\t"));
      Serial.print(batteryLevelKey(visit.batteryLevel));
      Serial.print(F("\t"));
      Serial.println(visit.version, DEC);
    }
  }

  Serial.println();
  // Show the details of each record
  {
    Serial.print(F("=============== "));
    Serial.print(recordCount, DEC);
    Serial.println(F(" records ==============="));
    Serial.println(F("stationId\tvitals\ttimestamp"));
    Serial.println(F("---------\t------\t---------"));
    for (int i = 0; i < recordCount; i++) {
      Serial.print(i, DEC);
      Serial.print(F(": "));
      int pageAddr = pageAddress(i);
      readIntoBuffer(pageAddr);
      char dataString[19];
      //                  stationId    vitals   timestamp
      sprintf(dataString, "%02X%02X\t\t%02X%02X\t%02X%02X%02X%02X", buffer[0],
              buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6],
              buffer[7]);
      Serial.print(dataString);

      Serial.println();
    }
  }
}

int TagIo::pageAddress(int zeroBasedRecordIndex) {
  // 1 page for the header, 2 pages for each record
  return START_PAGE_ADDR + 1 + (2 * zeroBasedRecordIndex);
}

bool TagIo::statusIsOk() { return status == MFRC522::STATUS_OK; }
bool TagIo::wipe() {
  byte pageData[PAGE_SIZE];
  for (byte i = 0; i < PAGE_SIZE; i++) {
    pageData[i] = RESET_CHAR;
  }

  bool succeeded = true;
  for (uint8_t page = START_PAGE_ADDR; page <= FINAL_PAGE_ADDR; page++) {
    succeeded &= writePage(page, pageData);
  }
  return succeeded;
}

bool TagIo::writePage(uint8_t pageAddr, byte *pageData) {
  status = (MFRC522::StatusCode)rfid->MIFARE_Ultralight_Write(
      pageAddr, pageData, PAGE_SIZE);
  return statusIsOk();
}

int TagIo::writeVisit(Visit visit) {
  int recordCount = numberOfVisits();
  if (recordCount == FAILED) {
    return FAILED;
  }
  // clobber the last record if records are full
  if (recordCount == MAX_RECORDS) {
    recordCount -= 1;
  }

  bool writeSuccess;
  byte pageAddr;
  // station id
  {
    unsigned int stationId = visit.stationId;

    byte stationData[PAGE_SIZE];
    stationData[0] = (stationId >> 8) & 0xFF;
    stationData[1] = stationId & 0xFF;
    // Plenty of opportunity to use vitals bytes more efficiently
    stationData[2] = visit.batteryLevel; // battery
    stationData[3] = visit.version;      // station software version
    pageAddr = pageAddress(recordCount);
    writeSuccess = writePage(pageAddr, stationData);
  }
  // timestamp
  {
    unsigned long timestamp = visit.timestamp;
    byte timestampData[PAGE_SIZE];
    timestampData[0] = (timestamp >> 24) & 0xFF;
    timestampData[1] = (timestamp >> 16) & 0xFF;
    timestampData[2] = (timestamp >> 8) & 0xFF;
    timestampData[3] = timestamp & 0xFF;
    writeSuccess &= writePage(pageAddr + 1, timestampData);
  }

  // only update the header with the new record count if succeeded
  if (writeSuccess) {
    byte headerData[PAGE_SIZE];
    headerData[FORMAT_VERSION_INDEX] = FORMAT_VERSION;
    headerData[1] = RESET_CHAR;
    headerData[RECORD_COUNT_INDEX] =
        recordCount + 1; // increment for this record
    headerData[3] = RESET_CHAR;
    writeSuccess &= writePage(START_PAGE_ADDR, headerData);
  }
  if (writeSuccess) {
    return recordCount; // not record count was before the write so it now
                        // equals the index of written
  } else {
    return FAILED;
  }
}

TagIo::Visit TagIo::readVisit(unsigned int zeroBasedRecordIndex) {
  int visitCount = numberOfVisits();
  if (zeroBasedRecordIndex >= visitCount || zeroBasedRecordIndex < 0) {
    return Visit();
  }
  Visit visit = Visit();
  readIntoBuffer(pageAddress(zeroBasedRecordIndex));

  // read station id - see corresponding write
  {
    unsigned int stationId;
    stationId = (uint32_t)buffer[0] << 8;
    stationId |= (uint32_t)buffer[1];
    visit.stationId = stationId;
  }

  {
    visit.batteryLevel = (TagIo::BatteryLevel)buffer[2];
    visit.version = (TagIo::BatteryLevel)buffer[3];
  }

  // read timestamp - see corresponding write
  {
    unsigned long timestamp;
    timestamp = (unsigned long)buffer[4] << 24;
    timestamp |= (unsigned long)buffer[5] << 16;
    timestamp |= (unsigned long)buffer[6] << 8;
    timestamp |= (unsigned long)buffer[7];
    visit.timestamp = timestamp;
  }

  return visit;
}
/**Called when a tag has been detected.  This returns the UID of the tag, in
 * uppercase HEX string.
 *
 */
String TagIo::uid() {
  char dataString[15]; // 7 byte / 14 characters + 1 terminating character?
  sprintf(dataString, "%02X%02X%02X%02X%02X%02X%02X", rfid->uid.uidByte[0],
          rfid->uid.uidByte[1], rfid->uid.uidByte[2], rfid->uid.uidByte[3],
          rfid->uid.uidByte[4], rfid->uid.uidByte[5], rfid->uid.uidByte[6]);

  return String(dataString);
}

String TagIo::batteryLevelKey(TagIo::BatteryLevel batteryLevel) {
  switch (batteryLevel) {
  case TagIo::BatteryLevel::RECHARGE:
    return "recharge";
  case TagIo::BatteryLevel::VOLTAGE_LOW:
    return "low";
  case TagIo::BatteryLevel::VOLTAGE_MEDIUM:
    return "medium";
  case TagIo::BatteryLevel::VOLTAGE_HIGH:
    return "high";
  default:
    return "unknown";
  }
}