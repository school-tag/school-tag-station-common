
#include "StationSound.h"
#include <Arduino.h>

StationSound::StationSound() {}

StationSound::StationSound(int pin) { buzzerPin = pin; }

void StationSound::ofSuccessfulVisit(int numberOfVisitsOnTag) {
  int totalBeepMillis = 1000;
  int pulseMillis = totalBeepMillis / numberOfVisitsOnTag;
  pulseMillis = max(250, pulseMillis); // anything quicker is hard to detect

  for (int i = 0; i < numberOfVisitsOnTag; i++) {
    sound(3000, pulseMillis);
  }
}

void StationSound::ofRepeatTag() { sound(500, 1000); }
void StationSound::sound(int frequency, int durationInMillis) {
  tone(buzzerPin, frequency, durationInMillis);
  // the tone is asynchronous non-blocking.
  // we block for ease of use since our tones don't require interrupt.
  delay(durationInMillis);
}
